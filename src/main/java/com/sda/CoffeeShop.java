package com.sda;

import com.sda.model.Extra;
import com.sda.model.Order;
import com.sda.model.Product;
import com.sda.model.drink.IceLatte;
import com.sda.service.OrderService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class CoffeeShop {
    private OrderService orderService;
    private Map<Long, Order> ordersPending;
    private Map<Long, Order> ordersDone;

    public CoffeeShop() {
        orderService = new OrderService();
        ordersPending = new HashMap<>();
        ordersDone = new HashMap<>();
    }

    public Order createOrder(List<Product> products, List<Extra> extras) {
        Order order = orderService.createOrder(products, extras);
        ordersPending.put(order.getOrderID(), order);
        return order;
    }

    public void orderDone(long orderID) {
        Order order = ordersPending.remove(orderID);
        ordersDone.put(orderID, order);
    }

    public Map<Long, Order> getOrdersPending() {
        return ordersPending;
    }

    public Map<Long, Order> getOrdersDone() {
        return ordersDone;
    }
}
