package com.sda.model;

import java.math.BigDecimal;
import java.util.List;

final public class Order {
    private final long orderID;
    private final List<Product> products;
    private final List<Extra> extras;
    private final BigDecimal price;

    public Order(long orderID, List<Product> products, List<Extra> extras, BigDecimal price) {
        this.orderID = orderID;
        this.products = products;
        this.extras = extras;
        this.price = price;
    }

    public long getOrderID() {
        return orderID;
    }

    public List<Product> getProducts() {
        return products;
    }

    public List<Extra> getExtras() {
        return extras;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
