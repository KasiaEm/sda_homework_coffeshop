package com.sda.model.drink;

import java.math.BigDecimal;

public class IceLatte extends Coffee {
    private int iceCubes;

    public IceLatte(boolean syrup, boolean soyMilk, int iceCubes) {
        super(BigDecimal.valueOf(8.5), syrup, soyMilk);
        this.iceCubes = iceCubes;
    }

    public int getIceCubes() {
        return iceCubes;
    }
}
