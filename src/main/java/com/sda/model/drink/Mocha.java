package com.sda.model.drink;

import java.math.BigDecimal;

public class Mocha extends Coffee {
    private boolean seasoning;

    public Mocha(boolean syrup, boolean soyMilk, boolean seasoning) {
        super(BigDecimal.valueOf(6.5), syrup, soyMilk);
        this.seasoning = seasoning;
    }

    public boolean isSeasoning() {
        return seasoning;
    }
}
