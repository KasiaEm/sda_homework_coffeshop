package com.sda.model.drink;

import java.math.BigDecimal;

public class IceTea extends Tea {
    private boolean mint;

    public IceTea(boolean syrup, int lemon, boolean mint) {
        super(BigDecimal.valueOf(5.5), syrup, lemon);
        this.mint = mint;
    }

    public boolean isMint() {
        return mint;
    }
}
