package com.sda.model.drink;

import com.sda.model.Product;

import java.math.BigDecimal;

public abstract class Drink extends Product {
    private boolean syrup;

    public Drink() {
    }

    public Drink(BigDecimal price, boolean syrup) {
        super(price);
        this.syrup = syrup;
    }

    public boolean isSyrup() {
        return syrup;
    }
}
