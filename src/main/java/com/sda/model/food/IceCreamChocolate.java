package com.sda.model.food;

import java.math.BigDecimal;

public class IceCreamChocolate extends IceCream {

    public IceCreamChocolate(boolean cream, int wafers) {
        super(BigDecimal.valueOf(9.0), cream, wafers);
    }
}
