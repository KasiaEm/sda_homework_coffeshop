package com.sda.model.food;

import java.math.BigDecimal;

public class Cheesecake extends Cake {
    private boolean freshFruits;

    public Cheesecake(boolean cream, boolean chocolateIcing, boolean freshFruits) {
        super(BigDecimal.valueOf(8.0), cream, chocolateIcing);
        this.freshFruits = freshFruits;
    }

    public boolean isFreshFruits() {
        return freshFruits;
    }
}
