package com.sda.model.food;

import java.math.BigDecimal;

public class IceCreamVanilla extends IceCream{

    public IceCreamVanilla(boolean cream, int wafers) {
        super(BigDecimal.valueOf(9.0), cream, wafers);
    }
}
