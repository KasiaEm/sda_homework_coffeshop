package com.sda.model;

import java.math.BigDecimal;

public abstract class Product {
    private BigDecimal price;

    public Product() {
    }

    public Product(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
