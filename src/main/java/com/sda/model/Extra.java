package com.sda.model;

import java.math.BigDecimal;

public enum Extra {
    SUGAR(BigDecimal.ZERO), MILK(BigDecimal.ONE), ESPRESSO(BigDecimal.ONE), COCOA(BigDecimal.ZERO), CREAM(BigDecimal.ONE), WAFER(BigDecimal.ONE);
    private BigDecimal price;

    Extra(BigDecimal price){
        this.price = price;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
