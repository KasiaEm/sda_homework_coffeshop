package com.sda.service;

import com.sda.EmptyOrderException;
import com.sda.model.Extra;
import com.sda.model.Order;
import com.sda.model.Product;

import java.math.BigDecimal;
import java.util.List;

public class OrderService {
    private long currentOrderNumber;

    public OrderService(){
        this.currentOrderNumber = 0;
    }

    public Order createOrder(List<Product> products, List<Extra> extras) throws EmptyOrderException {
        if(products==null || products!=null && products.isEmpty()) {
            throw new EmptyOrderException();
        }
        BigDecimal priceProducts = products.stream().map(Product::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal priceExtras = extras.stream().map(Extra::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
        Order order = new Order(currentOrderNumber, products, extras, priceProducts.add(priceExtras));
        currentOrderNumber++;
        return order;
    }
}
