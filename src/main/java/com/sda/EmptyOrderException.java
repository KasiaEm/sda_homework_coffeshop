package com.sda;

public class EmptyOrderException extends RuntimeException {

    public EmptyOrderException() {
        super("Empty order!");
    }
}
