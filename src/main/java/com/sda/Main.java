package com.sda;

import com.sda.model.Extra;
import com.sda.model.Order;
import com.sda.model.Product;
import com.sda.model.drink.IceTea;
import com.sda.model.drink.Mocha;
import com.sda.model.food.Brownie;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        CoffeeShop coffeeShop = new CoffeeShop();

        List<Product> products = new LinkedList<>();
        List<Extra> extras = new LinkedList<>();

        products.add(new IceTea(true, 1, true));
        products.add(new Brownie(false, true, false));
        extras.add(Extra.ESPRESSO);

        Order order = coffeeShop.createOrder(products, extras);

        Order order1 = coffeeShop.createOrder(
                Arrays.asList(new Mocha(true, false, false)),
                Arrays.asList(Extra.COCOA));

        System.out.println("Order ID: " + order.getOrderID() + " Order price: " + order.getPrice());
        System.out.println("Order ID: " + order1.getOrderID() + " Order price: " + order1.getPrice());
    }
}
